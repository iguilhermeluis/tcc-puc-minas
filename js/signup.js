window.onload = function () {
  document.querySelector("#form__login").onsubmit = async function (e) {
    e.preventDefault();
    let form = this;

    let emailBox = document.querySelector(".email__box");
    let passwordBox = document.querySelector(".password__box");

    let error_email = document.querySelector(".error-email");
    let error_password = document.querySelector(".error-password");
    let error_login = document.querySelector(".error-login");
    let btn_text = document.querySelector("#btn_text");

    if (form.email.value == "") {
      emailBox.classList.add("error");
      error_email.style.display = "block";
    } else {
      emailBox.classList.remove("error");
      error_email.style.display = "none";
    }

    if (form.password.value == "") {
      passwordBox.classList.add("error");
      error_password.style.display = "block";
    } else {
      passwordBox.classList.remove("error");
      error_password.style.display = "none";
    }

    if (btn_text.innerText == "Carregando...") {
      return;
    }

    if (form.email.value != "" && form.password.value != "") {
      btn_text.innerText = "Carregando...";

      await signup(form.email.value, form.password.value).then((res) => {
        console.log(res);
        login(form.email.value, form.password.value);
      });

      //form.submit();
    }
  };
};

async function login(email, password) {
  await axios
    .post("auth/auth.php", {
      email,
      password,
    })
    .then((response) => {
      console.log(response);
      window.location.reload();
    });
}

async function signup(email, password) {
  let uuid = uuidv4();
  return await axios.post("auth/new-user.php", {
    email,
    password,
    uuid,
  });
}

function parseJwt(token) {
  var base64Url = token.split(".")[1];
  var base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/");
  var jsonPayload = decodeURIComponent(
    window
      .atob(base64)
      .split("")
      .map(function (c) {
        return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2);
      })
      .join("")
  );

  return JSON.parse(jsonPayload);
}

async function authGoogle(token) {
  let email = parseJwt(token).email;
  let uuid = uuidv4();
  await signup(email, uuid).then((res) => {
    console.log(res);
    login(email, uuid);
  });
}

function handleCredentialResponse(response) {
  authGoogle(response.credential);
}

function uuidv4() {
  return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, (c) =>
    (
      c ^
      (crypto.getRandomValues(new Uint8Array(1))[0] & (15 >> (c / 4)))
    ).toString(16)
  );
}
