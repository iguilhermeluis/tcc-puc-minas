document.querySelector("#form_report").onsubmit = async function (e) {
  e.preventDefault();

  let form = this;
  let isValid = true;

  let btn_text = document.querySelector("#btn_text");
  let error_anexo = form.querySelector(".error-anexo");
  error_anexo.innerText = "Adicione um anexo";

  let time_course = new Date();
  time_course.setFullYear(form.ano.value);
  time_course.setDate(1);
  time_course.setMonth(form.mes.value - 1);

  let departament = form.departament.value;
  let category = form.category.value;
  let cost_center = form.cost_center.value;
  let type_invoice = form.type_invoice.value;

  error_anexo.style.display = "none";

  if (btn_text.innerText == "Cadastrando...") {
    return;
  }

  // valid if array cointain text

  document.querySelectorAll(".lista-uploads a").forEach((item) => {
    if (item.innerText.trim().includes("Enviando:")) {
      isValid = false;
      error_anexo.innerText = "Envio em andamento";
    }
  });

  if (
    window.uploadCompleted == false ||
    window.uploadCompleted == undefined ||
    !window.fileList
  ) {
    error_anexo.style.display = "block";
    isValid = false;
  }

  if (isValid) {
    btn_text.innerHTML = "Cadastrando...";

    let clear_form = () => {
      form.reset();
      window.uploadLink = "";
      window.uploadCompleted = false;
      document.querySelector(".lista-uploads").innerHTML = "";
      window.fileList = false;
      btn_text.innerHTML = "Cadastrar";
    };

    await axios
      .post("services/create_doc.php", {
        time_course: time_course,
        attachments: window.fileList,
        departament: departament,
        category: category,
        cost_center: cost_center,
        type_invoice: type_invoice,
      })
      .then(function (response) {
        clear_form();
        openSnackbar("Cadastrado com sucesso!");
      })
      .catch(function (error) {
        console.error(error);
        openSnackbar("Houve um erro ao cadastrar.");
      });
  }
};
