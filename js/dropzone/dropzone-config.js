// "myAwesomeDropzone" is the camelized version of the HTML element's ID
Dropzone.options.myAwesomeDropzone = {
    paramName: "file", // The name that will be used to transfer the file
    maxFilesize: 2, // MB
    accept: function(file, done) {
        console.warn("uploaded file:", file);
      if (file.name == "basic.css") {
        done("Naha, you don't.");
      }
      else { done(); }
    }
  };