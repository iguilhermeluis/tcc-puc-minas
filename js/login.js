window.onload = function () {
  document.querySelector("#form__login").onsubmit = async function (e) {
    e.preventDefault();
    let form = this;

    let emailBox = document.querySelector(".email__box");
    let passwordBox = document.querySelector(".password__box");

    let error_email = document.querySelector(".error-email");
    let error_password = document.querySelector(".error-password");
    let error_login = document.querySelector(".error-login");
    let btn_text = document.querySelector("#btn_text");

    if (form.email.value == "") {
      emailBox.classList.add("error");
      error_email.style.display = "block";
    } else {
      emailBox.classList.remove("error");
      error_email.style.display = "none";
    }

    if (form.password.value == "") {
      passwordBox.classList.add("error");
      error_password.style.display = "block";
    } else {
      passwordBox.classList.remove("error");
      error_password.style.display = "none";
    }

    if (btn_text.innerText == "Carregando...") {
      return;
    }

    if (form.email.value != "" && form.password.value != "") {
      btn_text.innerText = "Carregando...";

      await axios
        .post("auth/auth.php", {
          email: form.email.value,
          password: form.password.value,
        })
        .then((response) => {
          window.location.reload();
        })
        .catch((error) => {
          console.log(error);
          btn_text.innerText = "Entrar";
          error_login.style.display = "block";
          form.password.value = "";
        });

      //form.submit();
    }
  };
};

function parseJwt(token) {
  var base64Url = token.split(".")[1];
  var base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/");
  var jsonPayload = decodeURIComponent(
    window
      .atob(base64)
      .split("")
      .map(function (c) {
        return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2);
      })
      .join("")
  );

  return JSON.parse(jsonPayload);
}

async function authGoogle(token) {
  let email = parseJwt(token).email;
  await axios
    .post("auth/auth-google.php", {
      token,
      email,
    })
    .then((response) => {
      console.log(response);
      window.location.reload();
    })
    .catch((error) => {
      console.log(error);
      document.querySelector(".error-login").style.display = "block";
    });
}

function handleCredentialResponse(response) {
  authGoogle(response.credential);
}
