window.onload = function () {
  let btn = document.getElementById("toggleMenu");
  let menu = document.getElementById("menu");

  btn.addEventListener("click", function () {
    menu.classList.toggle("show-menu");
    if (btn.src.match("assets/imgs/menu_burger.svg")) {
      btn.src = "assets/imgs/icon_close.svg";
    } else {
      btn.src = "assets/imgs/menu_burger.svg";
    }
  });

  window.addEventListener("click", function (e) {
    if (e.target.id != "toggleMenu") {
      menu.classList.remove("show-menu");
      btn.src = "assets/imgs/menu_burger.svg";
    }
  });

  window.addEventListener("scroll", function () {
    menu.classList.remove("show-menu");
    btn.src = "assets/imgs/menu_burger.svg";
  });
};

function openSnackbar(text) {
  var x = document.getElementById("snackbar");
  x.innerText = text;
  x.className = "show";
  setTimeout(function () {
    x.className = x.className.replace("show", "");
  }, 3000);
}
