<?php
  session_start();
  $page = "logs";
  require_once "connection/connection.php";

  if(!isset($_SESSION['authenticated'])){
    echo "<script> window.location.replace('index.php'); </script>";
    session_destroy(); 
  } 
  if($_SESSION['level_user'] == 1){
    echo "<script> window.location.replace('listar-nf.php'); </script>";
  } 
?>
<!DOCTYPE html>
<html lang="pt-BR">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Arquiva Ai - Listar Logs</title>
    <?php include 'components/favicon.php'; ?>
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link
      href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;700&display=swap"
      rel="stylesheet"
    />
    <link
      rel="stylesheet"
      href="https://use.fontawesome.com/releases/v5.7.0/css/all.css"
      integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ"
      crossorigin="anonymous"
    />

    <link rel="stylesheet" href="bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.2/css/jquery.dataTables.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />

    
    <link rel="stylesheet" href="css/estilo.css" />

    <style>
        table, th, td {
        border: 1px solid var(--border-color);
        border-collapse: collapse;
        padding: 10px;
        }
        table a {
            margin-right: 10px;
        }
        .panel {
          overflow-y: scroll;
          min-height: 80vh;
        }
        </style>
  </head>
  <body>
    <?php require_once "components/header.php"; ?>

    <main class="container-main">

    <?php require_once "components/sidebar.php"; ?>

    <div class="box-content">
      <div class="container-list spacing-header">
       
        <div class="panel">
          <h2>Listar Logs</h2>
          <br/>
          <div class="loader-box">
            <div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
          </div>

          <div>
            <table>
              <thead>
                <tr>
                    <th>Data</th> 
                    <th>Ação</th> 
                    <th>Usuário</th>
                </tr>
              </thead>
              <tbody>
                <?php
                    $id_company = $_SESSION['id_company'];
                    $sql = "SELECT l.*, DATE_FORMAT(l.time_course, '%d/%m/%Y %H:%i:%s') as date,  u.username FROM log l INNER JOIN users u ON l.id_user = u.id WHERE l.id_company = $id_company  ORDER BY time_course DESC";
                    $result = $pdo->query($sql);
                    while($row = $result->fetch(PDO::FETCH_OBJ)){
                     
                        echo "<tr>";
                        echo "<td>".$row->date."</td>"; 
                        echo "<td>".$row->action."</td>"; 
                        echo "<td>".$row->username."</td>"; 
                        echo "</tr>";
                    }
                ?>
  
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    </main>
    <div id="snackbar"></div>
    <script src="js/script.js"></script>
 
    <script>
        function confirmarExclusao(id){
            var resposta = confirm("Deseja realmente excluir?");
            if(resposta){
                window.location.href = "excluir-tipo-documento.php?id="+id;
            }
        }
    </script>
                       

  </body>
</html>
