<?php
  session_start();
  require_once "connection/connection.php";

  if(!isset($_SESSION['authenticated'])){
    echo "<script> window.location.replace('index.php'); </script>";
    session_destroy();
   // header("Location: index.php"); 
  }
  $page = "usuarios";
    
  if($_SESSION['level_user'] == 1){
    echo "<script> window.location.replace('dashboard.php'); </script>";
  }
  
?>  
<!DOCTYPE html>
<html lang="pt-BR">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Arquiva Ai - Cadastrar usuário</title>
    <?php include 'components/favicon.php'; ?>
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link
      href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;700&display=swap"
      rel="stylesheet"
    />
    <link
      rel="stylesheet"
      href="https://use.fontawesome.com/releases/v5.7.0/css/all.css"
      integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ"
      crossorigin="anonymous"
    />
    
    <link rel="stylesheet" href="css/estilo.css?v=10" />
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css" />
  
  </head>
  <body>
   <?php require_once "components/header.php"; ?>

    <main class="container-main">
     <?php require_once "components/sidebar.php"; ?>
     <div class="box-content">
      <div class="container-list spacing-header " >
        <!-- <button btn>Cadastrar um novo relatório</button> -->
        <?php
          if($_SESSION['level_user'] > 1 && $page == "usuarios"){
            echo "<a btn href='listar-usuarios.php'>Listar usuários</a>";
          }
        ?>
       

        <div class="panel" >
          <h2>Cadastrar usuário</h2>

          <form id="form_report" action="services/register_user.php" method="POST" >
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label for="user">Usuário</label>
                    <input type="text" class="form-control" id="user" name="username" placeholder="Usuário" data-test="user" required>
                </div>
              </div>

              <div class="col-md-3">
                <div class="form-group">
                  <label for="password">Senha</label>
                    <input type="password"  class="form-control" id="password" name="password" placeholder="Senha" data-test="password" required>
                </div>
              </div>

              <div class="col-md-3">
                <div class="form-group">
                  <label for="confirm_password">Confirmar Senha</label>
                    <input type="password" class="form-control" id="confirm_password" name="confirm_password"  data-test="confirm-password" placeholder="Confirmar senha" required>
                </div>
              </div>

                <div class='col-md-3'>
                    <div class='form-group'>
                    <label>Permissão</label>
                    <select name='level_user' data-test="level-user"> 
                        <option value='1' selected>Visualizar</option> 
                        <option value='2'>Modificar</option> 
                    </select>
                    </div>
                </div> 
        
            </div>

            <div class="box-align-right" style="margin-top: 20px;">
            <span class="error-text error-password" style="margin-right: 20px">As senha devem ser iguais</span>

              <button class="button-form" data-test="btn-submit">
                <span id="btn_text" style="margin-left: -5px;">Cadastrar</span>
                <img
                  src="assets/imgs/icon_checked.svg"
                  alt="Icone redondo com uma seta branca apontando para direita"
                  height="30"
                  width="30"
                />
              </button>
            </div>
          </form>
        </div>
      </div>
      </div>
    </main>

    <div id="snackbar"></div>
    <script src="js/script.js?v=10"></script>
    <script>
      //confirmar se as senhas são iguais
      const password = document.querySelector('#password');
      const confirm_password = document.querySelector('#confirm_password');
      const form = document.querySelector('#form_report');
      let error = document.querySelector('.error-password');

      form.addEventListener('submit', (e) => {
        if (password.value !== confirm_password.value) {
          e.preventDefault();
          error.style.display = 'block';
          password.value = '';
          confirm_password.value = '';
        } else {
          error.style.display = 'none';
        }
        
      });
      
    </script>
    
  </body>
</html>
