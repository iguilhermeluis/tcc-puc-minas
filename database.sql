CREATE DATABASE arquivaai;
USE arquivaai;

CREATE TABLE IF NOT EXISTS company (
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    active TINYINT NOT NULL DEFAULT 1,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS company_department (
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    active TINYINT NOT NULL DEFAULT 1,
    id_company INT NOT NULL,
    FOREIGN KEY (id_company) REFERENCES company(id),
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS users (
    id INT NOT NULL AUTO_INCREMENT,
    username VARCHAR(255) NOT NULL,
    user_password VARCHAR(255) NOT NULL,
    level_user ENUM('1','2') NOT NULL DEFAULT 1,
    active TINYINT NOT NULL DEFAULT 1,
    uuid VARCHAR(255) NOT NULL,
    id_company INT NOT NULL,
    FOREIGN KEY (id_company) REFERENCES company(id),
    PRIMARY KEY (id)
);

CREATE TABLE  IF NOT EXISTS type_invoices (
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    active TINYINT NOT NULL DEFAULT 1,
    id_company INT NOT NULL,
    FOREIGN KEY (id_company) REFERENCES company(id),
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS type_document (
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    active TINYINT NOT NULL DEFAULT 1,
    id_company INT NOT NULL,
    FOREIGN KEY (id_company) REFERENCES company(id),
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS category (
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    active TINYINT NOT NULL DEFAULT 1,
    id_company INT NOT NULL,
    FOREIGN KEY (id_company) REFERENCES company(id),
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS cost_center (
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    active TINYINT NOT NULL DEFAULT 1,
    id_company INT NOT NULL,
    FOREIGN KEY (id_company) REFERENCES company(id),
    PRIMARY KEY (id)
);

CREATE TABLE  IF NOT EXISTS invoices (
    id INT NOT NULL AUTO_INCREMENT UNIQUE,
    link VARCHAR(255) NOT NULL,
    time_course DATE NOT NULL,
    observation VARCHAR(255) NOT NULL,
    create_by INT NOT NULL,
    type_invoice INT NOT NULL,
    category INT NOT NULL,
    cost_center INT NOT NULL,
    id_company INT NOT NULL,
    company_department INT NOT NULL,
    FOREIGN KEY (company_department) REFERENCES company_department(id),
    FOREIGN KEY (id_company) REFERENCES company(id),
    FOREIGN KEY (create_by) REFERENCES users(id),
    FOREIGN KEY (type_invoice) REFERENCES type_invoices(id),
    FOREIGN KEY (category) REFERENCES category(id),
    FOREIGN KEY (cost_center) REFERENCES cost_center(id),
    PRIMARY KEY (id)
);

CREATE TABLE  IF NOT EXISTS document (
    id INT NOT NULL AUTO_INCREMENT UNIQUE,
    link VARCHAR(255) NOT NULL,
    time_course DATE NOT NULL,
    observation VARCHAR(255) NOT NULL,
    create_by INT NOT NULL,
    type_document INT NOT NULL,
    category INT NOT NULL,
    cost_center INT NOT NULL,
    id_company INT NOT NULL,
    FOREIGN KEY (id_company) REFERENCES company(id),
    FOREIGN KEY (create_by) REFERENCES users(id),
    FOREIGN KEY (type_document) REFERENCES type_document(id),
    FOREIGN KEY (category) REFERENCES category(id),
    FOREIGN KEY (cost_center) REFERENCES cost_center(id),
    PRIMARY KEY (id)
);

CREATE TABLE  IF NOT EXISTS log (
    id INT NOT NULL AUTO_INCREMENT UNIQUE,
    time_course DATETIME NOT NULL,
    action VARCHAR(255) NOT NULL,
    id_company INT NOT NULL,
    id_user INT NOT NULL,
    FOREIGN KEY (id_user) REFERENCES users(id),
    FOREIGN KEY (id_company) REFERENCES company(id),
    PRIMARY KEY (id)
);
