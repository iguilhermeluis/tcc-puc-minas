<?php
  session_start();
  require_once "connection/connection.php";

  if(!isset($_SESSION['authenticated'])){
    echo "<script> window.location.replace('index.php'); </script>";
    session_destroy();
   // header("Location: index.php"); 
  }

  if(isset($_GET['profile'])){
    $page = "perfil";
  } else {
    $page = "usuarios";
  } 
 
  if(isset($_GET['id'])){
    $id = $_GET['id'];

    if($_SESSION['level_user'] == 1){
      $id = $_SESSION['id'];
    }

    $id_company = $_SESSION['id_company'];
    $sql = "SELECT * FROM users WHERE id = :id AND id_company = $id_company";
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':id', $id);
    $stmt->execute();
    $user = $stmt->fetch(PDO::FETCH_ASSOC);

    $username = $user['username'];
    $password = $user['user_password'];
    $level_user = $user['level_user'];
    $id = $user['id'];
 

    if(!$user){
      echo "<script> window.location.replace('listar-usuario.php'); </script>";
    }
 
  }
?>  
<!DOCTYPE html>
<html lang="pt-BR">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Arquiva Ai - Editar usuário</title>
        <?php include 'components/favicon.php'; ?><link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link
      href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;700&display=swap"
      rel="stylesheet"
    />
    <link
      rel="stylesheet"
      href="https://use.fontawesome.com/releases/v5.7.0/css/all.css"
      integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ"
      crossorigin="anonymous"
    />
    
    <link rel="stylesheet" href="css/estilo.css" />
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css" />
  
  </head>
  <body>
   <?php require_once "components/header.php"; ?>

    <main class="container-main">
     <?php require_once "components/sidebar.php"; ?>
     <div class="box-content">
      <div class="container-list spacing-header " >
         <?php
          if($_SESSION['level_user'] > 1 && $page == "usuarios"){
            echo "<a btn href='listar-usuarios.php'>Listar usuários</a>";
          }
        ?>
       

        <div class="panel" >
          <h2>Editar <?php echo $page; ?></h2>

          <form id="form_report" action="services/editer_user.php" method="POST" >
            

            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label for="user">Usuário</label>
                    <input type="text" value="<?php echo  $id; ?>" name="id"  hidden>
                    <input type="text" data-test="user" value="<?php echo $username; ?>" class="form-control" id="user" name="username" placeholder="Usuário" required>
                </div>
              </div>

              <div class="col-md-3">
                <div class="form-group">
                  <label for="password">Senha</label>
                    <input type="password" data-test="password" value="<?php echo $password; ?>" class="form-control" id="password" name="password" placeholder="Senha" required>
                </div>
              </div>

              <div class="col-md-3">
                <div class="form-group">
                  <label for="confirm_password">Confirmar Senha</label>
                    <input type="password" data-test="confirm-password" value="<?php echo $password; ?>" class="form-control" id="confirm_password" name="confirm_password" placeholder="Confirmar senha" required>
                </div>
              </div>

              <?php
                if($_SESSION['level_user'] > 1){
                  echo "<div class='col-md-3'>
                    <div class='form-group'>
                    <label>Permissão</label>
                    <select name='level_user' data-test='level-user'>";

                    if($level_user == 1){
                      echo "<option value='1' selected>Visualizar</option>";
                      echo "<option value='2'>Modificar</option>";
                    } else {
                      echo "<option value='1'>Visualizar</option>";
                      echo "<option value='2' selected>Modificar</option>";
                    } 

                  echo "</select>
                    </div>
                  </div>";
                } else {
                  echo "<input type='hidden' name='level_user' value='$level_user' data-test='level-user'>";
                }
              ?>
            </div>


            <div class="box-align-right" style="margin-top: 20px;">
            
            <span class="error-text error-password" style="margin-right: 20px">As senha devem ser iguais</span>
            <button class="button-form" data-test="btn-submit">
                <span id="btn_text" style="margin-left: -5px; "  >Salvar edição</span>
                <img
                  src="assets/imgs/icon_checked.svg"
                  alt="Icone redondo com uma seta branca apontando para direita"
                  height="30"
                  width="30"
                />
              </button> 
             
            </div>
          </form>
        </div>
      </div>
      </div>
    </main>

    <div id="snackbar"></div>
    <script src="js/script.js"></script>
    <script>
      //confirmar se as senhas são iguais
      const password = document.querySelector('#password');
      const confirm_password = document.querySelector('#confirm_password');
      const form = document.querySelector('#form_report');
      let error = document.querySelector('.error-password');

      form.addEventListener('submit', (e) => {
        if (password.value !== confirm_password.value) {
          e.preventDefault();
          error.style.display = 'block'; 
          password.value = '';
          confirm_password.value = '';
        } else {
          error.style.display = 'none';
        }
      });
      
    </script>
    
  </body>
</html>
