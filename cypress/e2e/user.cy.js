describe("cc", () => {
  const random = Math.floor(Math.random() * 1000);
  const value = `TESTE-${random}`;

  beforeEach(() => {
    cy.visit("http://localhost/arquiva-ai/index.php");
    cy.get("[data-test=email]").type("teste@admin.com");
    cy.get("[data-test=password]").type("admin");
    cy.get("[data-test=submit]").click();
  });

  it("should register an user", () => {
    cy.get('[data-test="menu-usuarios"]').click();
    cy.get('[data-test="btn-add-item"]').click();
    cy.get('[data-test="user"]').type(value);
    cy.get('[data-test="password"]').type("123456");
    cy.get('[data-test="confirm-password"]').type("123456");
    cy.get("select").select("Visualizar").should("have.value", "1");
    cy.get("[data-test=btn-submit]").click();
    cy.get(`[data-test="td-${value}"]`).should("contain", value);
    cy.get(`[data-test="edit-${value}"]`).click();
  });

  it("should edit an user", () => {
    cy.get('[data-test="menu-usuarios"]').click();
    cy.get(`[data-test="edit-${value}"]`).click();
    cy.get('[data-test="user"]').clear().type(`${value}-EDIT`);
    cy.get('[data-test="password"]').type("654321");
    cy.get('[data-test="confirm-password"]').type("654321");
    cy.get("select").select("Modificar").should("have.value", "2");
    cy.get("[data-test=btn-submit]").click();
    cy.get(`[data-test="td-${value}-EDIT"]`).should("contain", `${value}-EDIT`);
  });

  it("should remove an user", () => {
    cy.get('[data-test="menu-usuarios"]').click();
    cy.get(`[data-test="remove-${value}-EDIT"]`).click();
    cy.get(`[data-test="td-${value}-EDIT"]`).should("not.exist");
  });
});
