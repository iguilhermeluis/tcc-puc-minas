describe("Menu", () => {
  const menuItems = [
    "Visão geral",
    "Gerenciamento de documentos",
    "Gerenciamento de notas fiscais",
    "Categorias",
    "Centros de custos",
    "Tipos faturas",
    "Tipos documentos",
    "Departamentos",
    "Usuários",
    "Editar perfil",
    "Editar empresa",
    "Ver logs",
    "Sair",
  ];

  beforeEach(() => {
    cy.visit("http://localhost/arquiva-ai/index.php");
    cy.get("[data-test=email]").type("teste@admin.com");
    cy.get("[data-test=password]").type("admin");
    cy.get("[data-test=submit]").click();
  });

  it("should display all menu items", () => {
    cy.url().should("include", "/dashboard");
    cy.get("#menu").find("li").should("have.length", 13);
  });

  it("should highlight the active menu item", () => {
    cy.visit("http://localhost/arquiva-ai/dashboard.php");

    const menus = [
      "menu-dashboard",
      "menu-nf",
      "menu-categorias",
      "menu-cc",
      "menu-documentos",
      "menu-tipo-fatura",
      "menu-tipo-documento",
      "menu-departamento",
      "menu-usuarios",
      "menu-perfil",
      "menu-editar-empresa",
      "menu-logs",
      "menu-logout",
    ];

    menus.forEach((menu) => {
      const menuSelector = cy.get(`[data-test=${menu}]`);
      if (menu === "menu-dashboard") {
        menuSelector.should("have.class", "active");
      } else {
        menuSelector.should("not.have.class", "active");
      }
    });
  });

  function clickMenuItem(menuItem) {
    cy.get('[data-test="menu"]').contains(menuItem).click();
  }

  function navigateMenu(menuItems) {
    if (menuItems.length === 0) return;

    const currentMenuItem = menuItems[0];

    clickMenuItem(currentMenuItem);
    cy.get("body").should("not.contain", "Loading...");

    const remainingMenuItems = menuItems.slice(1);
    navigateMenu(remainingMenuItems);
  }

  describe("Menu Navigation", () => {
    it("should navigate through all menu items", () => {
      navigateMenu(menuItems);
    });
  });
});
