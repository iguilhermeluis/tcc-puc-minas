describe("departament", () => {
  const random = Math.floor(Math.random() * 1000);
  const value = `TESTE-${random}`;

  beforeEach(() => {
    cy.visit("http://localhost/arquiva-ai/index.php");
    cy.get("[data-test=email]").type("teste@admin.com");
    cy.get("[data-test=password]").type("admin");
    cy.get("[data-test=submit]").click();
  });

  it("should register a departament", () => {
    cy.get('[data-test="menu-departamento"]').click();
    cy.get('[data-test="btn-add-item"]').click();
    cy.get('[data-test="title"]').type(value);
    cy.get("[data-test=btn-submit]").click();
    cy.get(`[data-test="td-${value}"]`).should("contain", value);
    cy.get(`[data-test="edit-${value}"]`).click();
  });

  it("should edit a departament", () => {
    cy.get('[data-test="menu-departamento"]').click();
    cy.get(`[data-test="edit-${value}"]`).click();
    cy.get('[data-test="title"]').clear().type(`${value}-EDIT`);
    cy.get("[data-test=btn-submit]").click();
    cy.get(`[data-test="td-${value}-EDIT"]`).should("contain", `${value}-EDIT`);
  });

  it("should remove a departament", () => {
    cy.get('[data-test="menu-departamento"]').click();
    cy.get(`[data-test="remove-${value}-EDIT"]`).click();
    cy.get(`[data-test="td-${value}-EDIT"]`).should("not.exist");
  });
});
