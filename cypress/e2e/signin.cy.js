describe("Signin", () => {
  beforeEach(() => {
    cy.visit("http://localhost/arquiva-ai/index.php");
  });
  it("should allow login with valid credentials", () => {
    cy.get("[data-test=email]").type("teste@admin.com");
    cy.get("[data-test=password]").type("admin");
    cy.get("[data-test=submit]").click();
    cy.url().should("include", "/dashboard");
  });

  it("Should display an error message for invalid credentials", () => {
    cy.get("[data-test=email]").type("nonexistent_user@admin.com"); // Enter an invalid email
    cy.get("[data-test=password]").type("wrong_password"); // Enter an incorrect password
    cy.get("[data-test=submit]").click(); // Click the login button
    cy.get("[data-test=login-error]").should("be.visible"); // Verify if the error message is displayed on the screen
  });
});
