describe("Signup", () => {
  beforeEach(() => {
    cy.visit("http://localhost/arquiva-ai/cadastrar.php");
  });
  it("should signup a new user", () => {
    cy.get("[data-test=email]").type("teste@admin.com");
    cy.get("[data-test=password]").type("admin");
    cy.get("[data-test=submit]").click();
    cy.url().should("include", "/dashboard");
  });
});
