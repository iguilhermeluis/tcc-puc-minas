<?php
  session_start();
  require_once "connection/connection.php";

  if(!isset($_SESSION['authenticated'])){
    echo "<script> window.location.replace('index.php'); </script>";
    session_destroy();
   // header("Location: index.php"); 
  }

  if(isset($_GET['profile'])){
    $page = "perfil";
  } else {
    $page = "usuarios";
  } 
 
  if(isset($_GET['id'])){
    $id = $_GET['id'];
    if($_SESSION['level_user'] == 1){
      $id = $_SESSION['id'];
    }
    $id_company = $_SESSION['id_company'];
    $sql = "SELECT * FROM type_document WHERE id = :id AND id_company = $id_company";
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':id', $id);
    $stmt->execute();
    $user = $stmt->fetch(PDO::FETCH_ASSOC);

    $username = $user['name']; 
    $id = $user['id'];
 
    if(!$user){
      echo "<script> window.location.replace('listar-nf.php'); </script>";
    }
  }
?>  
<!DOCTYPE html>
<html lang="pt-BR">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Arquiva Ai - Editar tipo de documentos</title>
        <?php include 'components/favicon.php'; ?><link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link
      href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;700&display=swap"
      rel="stylesheet"
    />
    <link
      rel="stylesheet"
      href="https://use.fontawesome.com/releases/v5.7.0/css/all.css"
      integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ"
      crossorigin="anonymous"
    />
    
    <link rel="stylesheet" href="css/estilo.css" />
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css" />
  
  </head>
  <body>
   <?php require_once "components/header.php"; ?>

    <main class="container-main">
     <?php require_once "components/sidebar.php"; ?>
     <div class="box-content">
      <div class="container-list spacing-header " >
        <!-- <button btn>Cadastrar um novo relatório</button> -->
         <a btn href='listar-tipo-documento.php'>Listar tipos documentos</a> 
       

        <div class="panel" >
          <h2>Editar documento</h2>

          <form id="form_report" action="services/editer_type_doc.php" method="POST" >
             
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label for="user">Tipo de documento</label>
                    <input type="text" value="<?php echo  $id; ?>" name="id"  hidden>
                  <input type="text" value="<?php echo $username; ?>" data-test="title" class="form-control" id="user" name="username" placeholder="Documento" required>
                </div>
              </div>  
            </div> 

            <div class="box-align-right" style="margin-top: 20px;"> 
                <button class="button-form" style="width: 170px;" data-test="btn-submit">
                <span id="btn_text" style="margin-left: -5px; ">Salvar edição</span>
                <img
                  src="assets/imgs/icon_checked.svg"
                  alt="Icone redondo com uma seta branca apontando para direita"
                  height="30"
                  width="30"
                />
              </button> 
             
            </div>
          </form>
        </div>
      </div>
      </div>
    </main>

    <div id="snackbar"></div>
    <script src="js/script.js"></script>
 
    
  </body>
</html>
