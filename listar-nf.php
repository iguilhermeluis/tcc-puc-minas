<?php
  session_start();
  require_once "connection/connection.php";

  $page = "faturas";

  if(!isset($_SESSION['authenticated'])){
    echo "<script> window.location.replace('index.php'); </script>";
    session_destroy();
   // header("Location: index.php"); 
  }
?>
<!DOCTYPE html>
<html lang="pt-BR">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Arquiva Ai - Listar notas fiscais</title>
    <?php include 'components/favicon.php'; ?>
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link
      href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;700&display=swap"
      rel="stylesheet"
    />
    <link
      rel="stylesheet"
      href="https://use.fontawesome.com/releases/v5.7.0/css/all.css"
      integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ"
      crossorigin="anonymous"
    />
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.2/css/jquery.dataTables.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
    <link rel="stylesheet" href="css/estilo.css" />
  </head>
  <body>
    <?php require_once "components/header.php"; ?>

    <main class="container-main">
    <?php require_once "components/sidebar.php"; ?>
    <div class="box-content">
      <div class="container-list spacing-header">

      <?php
        if($_SESSION['level_user'] > 1){
          echo "<a href='cadastrar-nf.php' btn>Cadastrar notas fiscais</a>";
        }
      ?>
     

        <div class="panel lista-relatorios">
          <h2>Listar notas fiscais</h2>

          <div class="filter-panel">
            <form>
              <div class="row" class="panel-filter">
               
                <div class="col-md-3">
                  <div class="form-group">
                    <label>MÊS</label>
                    <select name="mes">
                    <?php
                      $meses = array('Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro');
                      for ($i = 0; $i < 12; $i++) {
                        echo '<option value="' . ($i + 1) . '">' . $meses[$i] . '</option>';
                      }
                    ?>
                    </select>
                  </div>
                </div>

                <div class="col-md-3">
                  <div class="form-group">
                    <label>ANO</label>
                    <select name="ano"> 
                      <?php
                        for ($i = date('Y'); $i >= 2010; $i--) {
                          echo "<option value='$i'>$i</option>";
                        }
                      ?>
                    </select>
                  </div>
                </div>
                      
                <!-- create checkbox for filter year only -->
                <div class="col-md-3">
                  <div class="form-group checkbox_text">
                    <label><input type="checkbox" name="ano_checkbox" value="ano_checkbox">FILTRAR SOMENTE ANO</label>
                  </div>
                </div>
               
                <div class="col-md-3" >
                  <button id="btn-filter-table" btn btn-filter style="margin-top: 30px">Filtrar</button>
                  <button id="btn-filter-reset" btn btn-filter style="margin-top: 30px; background: #ed5153; border-color: #ed5153; display:none">Limpar Filtro</button>
                </div>
              </div>
            </form>
          </div>


          <div class="loader-box">
            <div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
          </div>

          <div class="table-container">
            <table id="basic">
              <thead>
                <tr>
                  <th>Nome</th>
                  <th>Mês / Ano</th>
                  <th>Tipo</th>
                  <th>Departamento</th>
                  <th>Centro de Custo</th>
                  <th>Categoria</th>
                  <th>Anexos</th>
                </tr>
              </thead>
              <tbody>
  
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    </main>
    <div id="snackbar"></div>
    <script src="js/script.js"></script>
    <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>

    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.2/js/jquery.dataTables.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>                    

    <script>
      
    <?php
        $id_company = $_SESSION['id_company'];
        $sql = "SELECT i.id, link, time_course, observation, DATE_FORMAT(`time_course`,'%m') AS month, year(time_course) as YEAR, ti.name AS type_invoice,
        c.name AS category, co.name AS cost_center, cd.name AS company_department FROM invoices i 
        INNER JOIN type_invoices ti ON i.type_invoice = ti.id
        INNER JOIN category c ON i.category = c.id
        INNER JOIN cost_center co ON i.cost_center = co.id
        INNER JOIN company_department  cd ON i.company_department = cd.id
        WHERE i.id_company = $id_company
        ORDER BY year DESC";
        $result = $pdo->query($sql);
        $array = $result->fetchAll( PDO::FETCH_ASSOC );
        echo "var reports = ".json_encode($array).";";
        ?>

      var GLOBAL_TABLE;
      $(document).ready(function () {

        //initial state
        let today = new Date();
        let current_month = today.getMonth() + 1;
        let current_year = today.getFullYear();
      
        let filtered = reports.filter(element => {
          let time_course = element.time_course.split('-');
          return time_course[1] == current_month && time_course[0] == current_year;
        });

        buildTable(filtered);
        
        listenerFilter();
      });
    </script>


<script>
 
 function create_table(id){
    GLOBAL_TABLE = $(id).DataTable({
      responsive: true,
      destroy: true,
      language: {
        url: "//cdn.datatables.net/plug-ins/1.10.24/i18n/Portuguese-Brasil.json",
        search: "_INPUT_",
        searchPlaceholder: "Pesquisar..."
      },
      order: [[1, "desc"]],
      "autoWidth": false,
      "columnDefs": [
      { "width": "150px", "targets": 0 },
      { "width": "100px", "targets": 1 },
      { "width": "100px", "targets": 2 },
      { orderable: false,  targets: [2] }
      ],
      fixedColumns: true
    });
 }

  function listenerFilter() {	
    const month = document.querySelector('[name="mes"]');
    const year = document.querySelector('[name="ano"]');
    const btnFilter = document.querySelector('#btn-filter-table');
    const btnReset = document.querySelector('#btn-filter-reset');
    const checkbox = document.querySelector('[name="ano_checkbox"]');
    
    checkbox.addEventListener('change', function () {
      if (this.checked) {
        month.disabled = true;
      } else {
        month.disabled = false;
      }
      btnFilter.click();
    }); 
    btnReset.addEventListener('click', function (e) {
      e.preventDefault();
      this.style.display = 'none';
      month.disabled = false;
      checkbox.checked = false;
      buildTable(reports, true);
    }); 
    month.addEventListener('change', function() {
      btnFilter.click();
    });
    year.addEventListener('change', function() {
      btnFilter.click();
    });
    btnFilter.addEventListener('click', (e) => {
      e.preventDefault();
      filter(reports)
      btnReset.style.display = 'inline-block';
    });

     //select mouth and year
     
      let today = new Date();
      let current_month = today.getMonth() + 1;
      let current_year = today.getFullYear();
      month.value = current_month;
      year.value = current_year;
      
  }

  //confirm delete
  function confirmDelete(e){ 
    var url = e.getAttribute('data-url');
    var x = confirm("Deseja realmente excluir este registro?");
    if (x) {
      window.location.href = url;
    } else { 
      return false;
    }
  }

  function buildTable(data, update = false) {


    let loader = document.querySelector('.loader-box');
    loader.style.display = 'flex';
    let table_container = document.querySelector('.table-container');
    table_container.style.display = 'none';
    if(update) {
          GLOBAL_TABLE.destroy();
        } 
        let tbody = document.querySelector('#basic tbody');
        let content = "";

        data.forEach(element => {
          let time_course = element.time_course.split('-');
          content += `
          <tr>
            <td><span class="title-mobile">Nome:</span> ${element.observation}</td>
            <td><span class="title-mobile">Mês/Ano:</span> ${time_course[1]}/${time_course[0]}</td>
            <td><span class="title-mobile">Departamento:</span> ${element.company_department}</td>
            <td><span class="title-mobile">Centro de custo:</span> ${element.cost_center}</td>
            <td><span class="title-mobile">Categoria:</span> ${element.category}</td>
            <td><span class="title-mobile">Tipo:</span> ${element.type_invoice}</td>
       
            <td>
              <span class="title-mobile">Anexos:</span>
              <span class="buttons">

              <a href="services/view.php?file=${element.observation}" rel='modal:open'>
              
                <img
                  src="assets/imgs/icon_pdf.svg"
                  alt="pdf"
                  title="Visualizar PDF"
                  />
              </a>

              <a href="services/download.php?file=${element.observation}" target="_blank" download class="btn btn-primary">
              <img
                  src="assets/imgs/icon_download.svg"
                  title="Baixar PDF"
                  alt="download"
                />
              </a>

              <?php
                  if($_SESSION["level_user"] > 1){
                    echo '
                    <a onClick="confirmDelete(this)"  data-url="excluir-fatura.php?id=${element.id}" style="cursor:pointer">
                    <img
                        src="assets/imgs/icon_delete.svg"
                        title="Excluir esse relatório"
                        alt="excluir"
                      />
                    </a>';
                  }
              ?> 
              </span>
            </td>
          </tr>`;
        });
        tbody.innerHTML = content;

        create_table('#basic');

    setTimeout(() => {
        loader.style.display = 'none';
        table_container.style.display = 'block';
    }, 750);
  }

  function filter(data) {
      // padstart
      let month =  document.querySelector('select[name="mes"]').value.padStart(2, "0");
      let year = document.querySelector('select[name="ano"]').value;
      let ano_checkbox = document.querySelector('input[name="ano_checkbox"]').checked;

      let filtered = data.filter(element => {
        let time_course = element.time_course.split('-');
        if(ano_checkbox) {
          return time_course[0] == year;
        } 
        
        return time_course[1] == month && time_course[0] == year;
      });
      buildTable(filtered, true); 
  }

    </script>

  
  </body>
</html>
