<?php  
$usr = "root";
$pwd = "";
$hos = 'mysql:host=localhost;dbname=arquivaai;charset=utf8'; 

$options = array(
    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8'
);
try {
    $pdo = new PDO($hos, $usr, $pwd,$options);
} catch (PDOException $ex) {
    echo "Error ".$ex->getMessage();
}

?>
