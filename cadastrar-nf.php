<?php
  session_start();
  $page = "faturas";
  require_once "connection/connection.php";
  if(!isset($_SESSION['authenticated'])){
    echo "<script> window.location.replace('index.php'); </script>";
    session_destroy();
   // header("Location: index.php"); 
  }

  if($_SESSION['level_user'] == 1){
    echo "<script> window.location.replace('listar-nf.php'); </script>";
  }
?>
<!DOCTYPE html>
<html lang="pt-BR">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Arquiva Ai - Cadastrar Fatura</title>
    <?php include 'components/favicon.php'; ?>
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link
      href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;700&display=swap"
      rel="stylesheet"
    />
    <link
      rel="stylesheet"
      href="https://use.fontawesome.com/releases/v5.7.0/css/all.css"
      integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ"
      crossorigin="anonymous"
    />
    
    <link rel="stylesheet" href="css/estilo.css?v=9" />
    <link rel="stylesheet" href="js/upload/upload.css" />
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css" />
  
  </head>
  <body>
   <?php require_once "components/header.php"; ?>

    <main class="container-main">
     <?php require_once "components/sidebar.php"; ?>
     <div class="box-content">
      <div class="container-list spacing-header " >
        <!-- <button btn>Cadastrar um novo relatório</button> -->
        <a btn href="listar-nf.php">Listar notas fiscais</a>

        <div class="panel" >
          <h2>Cadastrar nota fiscal</h2>

          <form id="form_report">
            

            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label for="mes">Mês</label>
                  <select name="mes" >
                    <?php
                      $meses = array('Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro');
                      for ($i = 0; $i < 12; $i++) {
                        echo '<option value="' . ($i + 1) . '">' . $meses[$i] . '</option>';
                      }
                    ?>
                  </select>
                </div>
              </div>

              <div class="col-md-3">
                <div class="form-group">
                  <label>Ano</label>
                  <select name="ano">
                    <?php
                      for ($i = date('Y'); $i >= 2010; $i--) {
                        echo "<option value='$i'>$i</option>";
                      }
                    ?>
                  </select>
                </div>
              </div>

              <div class="col-md-3">
                <div class="form-group">
                  <label>Tipo nota fiscal</label>
                  <select name="type_invoice">
                    <?php 
                      $id_company = $_SESSION['id_company'];
                      $sql = "SELECT id, name FROM type_invoices WHERE id_company = $id_company ORDER BY name ASC";
                      $result = $pdo->query($sql); 
                      while($row = $result->fetch(PDO::FETCH_OBJ)){
                        echo "<option value='$row->id'>$row->name</option>";
                      } 
                    ?>
                  </select>
                </div>
              </div>

              <div class="col-md-3">
                <div class="form-group">
                  <label>Centro de Custo</label>
                  <select name="cost_center">
                  <?php 
                      $sql = "SELECT id, name FROM cost_center WHERE id_company = $id_company ORDER BY name ASC";
                      $result = $pdo->query($sql); 
                      while($row = $result->fetch(PDO::FETCH_OBJ)){
                        echo "<option value='$row->id'>$row->name</option>";
                      } 
                    ?>
                  </select>
                </div>
              </div>

              <div class="col-md-3">
                <div class="form-group">
                  <label>Categoria</label>
                  <select name="category">
                  <?php 
                      $sql = "SELECT id, name FROM category WHERE id_company = $id_company ORDER BY name ASC";
                      $result = $pdo->query($sql); 
                      while($row = $result->fetch(PDO::FETCH_OBJ)){
                        echo "<option value='$row->id'>$row->name</option>";
                      } 
                    ?>
                  </select>
                </div>
              </div>


              <div class="col-md-3">
                <div class="form-group">
                  <label>Departamento</label>
                  <select name="departament">
                  <?php 
                      $sql = "SELECT id, name FROM company_department WHERE id_company = $id_company ORDER BY name ASC";
                      $result = $pdo->query($sql); 
                      while($row = $result->fetch(PDO::FETCH_OBJ)){
                        echo "<option value='$row->id'>$row->name</option>";
                      } 
                    ?>
                  </select>
                </div>
              </div>

              
             
              <div class="col-md-6">
                <div class="area-upload">
                  <label for="upload-file" class="label-upload">
                    <i class="fas fa-cloud-upload-alt"></i>
                    <div class="texto">Clique ou arraste o arquivo</div>
                  </label>
                  <input
                    type="file"
                    accept="application/pdf"
                    id="upload-file"
                    multiple
                  />
                  <div class="lista-uploads"></div>
                </div>
                <span class="error-text error-anexo">Adicione um anexo</span>
              </div>
            </div>

            <div class="box-align-right">
              <button class="button-form">
                <span id="btn_text" style="margin-left: -15px;">Cadastrar</span>
                <img
                  src="assets/imgs/icon_checked.svg"
                  alt="Icone redondo com uma seta branca apontando para direita"
                  height="30"
                  width="30"
                />
              </button>
            </div>
          </form>
        </div>
      </div>
      </div>
    </main>

    <div id="snackbar"></div>
    <script src="js/script.js"></script> 
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="js/upload/upload_invoice.js"></script>
    <script src="js/cadastrar-fatura.js"></script>
    <script>
    
    var mes = document.querySelector('select[name="mes"]');
    var ano = document.querySelector('select[name="ano"]');
    var mes_atual = new Date().getMonth() + 1;
    var ano_atual = new Date().getFullYear();
    mes.value = mes_atual;
    ano.value = ano_atual;
    </script>
  </body>
</html>
