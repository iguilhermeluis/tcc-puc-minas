<?php
session_start();
if(isset($_SESSION['authenticated'])){
  echo "<script> window.location.replace('dashboard.php'); </script>";
} 
?>

<!DOCTYPE html>
<html lang="pt-BR">
  <head>
    <meta charset="UTF-8" />
    <meta name="robots" content="noindex">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Arquiva Ai - Sistema Web</title> 
    <meta name="description" content="Sistema Web para Arquivamento de Documentos" />
    <?php include 'components/favicon.php'; ?>
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link
      href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;700&display=swap"
      rel="stylesheet"
    />
    <link rel="stylesheet" href="css/estilo.css" />
    <meta name="author" content="Guilherme Luis Faustino" />
    <script src="https://accounts.google.com/gsi/client" async defer></script>
    <div id="g_id_onload"
         data-client_id="359371269438-uhs476r2d8to6vqoh4i8ahih1oit2fmr"
         data-callback="handleCredentialResponse">
    </div>
   
 
  </head>
  <body>
    <div class="container__login">
      <div class="box_login">
        <h1>Arquiva Ai</h1>
        <h2>Novo usuário</h2>
        <form id="form__login">
          <div class="row">
            <div class="input__login email__box">
              <label>Usuário:</label>
              <input type="text" data-test="email" name="email" placeholder="Digite seu e-mail" />
            </div>
            <span class="error-text error-email">Digite o e-mail</span>
          </div>

          <div class="row">
            <div class="input__login password__box">
              <label>Senha:</label>
              <input
                type="password"
                name="password"
                data-test="password"
                placeholder="Digite sua senha"
              />
            </div>
            <span class="error-text error-password">Digite a senha</span>
          </div>

          <div class="footer__login">
            <!-- alert error -->
             <div class="g_id_signin" data-type="standard"></div>
            <button data-test="submit" class="button__login">
              <img
                src="assets/imgs/icon_login.svg"
                alt="Icone redondo com uma seta branca apontando para direita"
                height="30"
                width="30"
              />
              <span id="btn_text">Cadastrar</span>
            </button>
          </div>

      
          <p id="msg" class="error-text error-login"> E-mail e/ou senha inválidos</p>
           
          <a href="index.php" class="link__login">Ja tenho cadastro,  <span>fazer login</span></a>
        </form>
      </div>
    </div>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="js/signup.js?v=4"></script>
 	
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  </body>
</html>
