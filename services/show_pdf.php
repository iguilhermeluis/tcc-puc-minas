<?php
session_start();

require_once "jwt.php";

if(isset($_GET['file'])){
    $path ="/home/pedenet/www/tcc/uploads/";
    $fileName = filter_var($_GET['file'], FILTER_SANITIZE_STRING);
    $finalPath = $path.$fileName;

    header('Content-Type: application/pdf');
    header("Content-Disposition: inline; filename=$finalPath;");
    readfile($finalPath);
} else {
    exit('You are not authorized to access this file.');
}

?>
