<?php 

//Application Secret
function get_token($user) {
    //Header Token
    $header = [
        'typ' => 'JWT',
        'alg' => 'HS256'
    ];

    //Payload - Content
    $payload = [
        'exp' => (new DateTime("now"))->getTimestamp(),
        'uid' => $user, 
    ];

    //JSON
    $header = json_encode($header);
    $payload = json_encode($payload);

    //Base 64
    $header = base64_encode($header);
    $payload = base64_encode($payload);

    //Sign
    $sign = hash_hmac('sha256', $header . "." . $payload, "@PUCMINAS", true);
    $sign = base64_encode($sign);  
   return  $header . '.' . $payload . '.' . $sign;
}
 
function verify_token($token) {
    $part = explode(".",$token);
    $header = $part[0];
    $payload = $part[1];
    $signature = $part[2];
    
    $valid = hash_hmac('sha256',"$header.$payload",'@PUCMINAS',true);
    $valid = base64_encode($valid);

    return ($valid === $signature);
}

?>