<?php
    session_start();
    require_once '../connection/connection.php';
    require_once '../services/register_log.php';

    if(isset($_SESSION['authenticated'])){

        $_POST = json_decode(file_get_contents("php://input"),true);
        
        $attachments = $_POST['attachments'];
        $time_course = $_POST["time_course"]; 
        $create_by = $_SESSION['id'];
        $category = $_POST["category"];
        $cost_center = $_POST["cost_center"];
        $type_invoice = $_POST["type_invoice"]; 
        $id_company = $_SESSION['id_company'];
        $departament = $_POST["departament"];
 
        foreach($attachments as $attachment){
            $observation = $attachment['name'];
            $link = $attachment['path'];
     

            $sql = "REPLACE INTO document (link, time_course, observation, create_by, category, cost_center, type_document, id_company) VALUES (:link, :time_course, :observation, :create_by, :category, :cost_center, :type_document, :id_company)";

            $stmt = $pdo->prepare($sql);
            $stmt->bindparam(':link', $link, PDO::PARAM_STR);
            $stmt->bindparam(':time_course', $time_course, PDO::PARAM_STR);
            $stmt->bindparam(':observation', $observation, PDO::PARAM_STR);
            $stmt->bindparam(':create_by', $create_by, PDO::PARAM_STR);
            $stmt->bindparam(':category', $category, PDO::PARAM_STR);
            $stmt->bindparam(':cost_center', $cost_center, PDO::PARAM_STR);
            $stmt->bindparam(':type_document', $type_invoice, PDO::PARAM_STR);
            $stmt->bindparam(':id_company', $id_company, PDO::PARAM_STR); 
            $stmt->execute();
    
            if ($stmt->rowCount()) { 
                create_log($pdo, "Criação de documento");
                http_response_code(200); 
            } else { 
                http_response_code(422);
            }
        }

    } else {
        http_response_code(401);
    } 


  
 
    
?>
