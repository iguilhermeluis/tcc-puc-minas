<?php
session_start();

if(isset($_SESSION['authenticated'])){
    $file = $_FILES['file'];
    $uploaddir ="/home/pedenet/www/tcc/uploads/"; 
    $clear_name = str_replace(" ", "-", $file['name']);
    $uploadfile = $uploaddir . basename($clear_name);
    $ret = [];
    
    if (move_uploaded_file($file['tmp_name'], $uploadfile)) {
        $ret["status"] = "success";
        $ret["path"] = 'uploads/'.  $clear_name;
        $ret["name"] =  $clear_name;
    } else {
        $ret["status"] = "error";
        $ret["name"] =  $clear_name;
    }
    echo json_encode($ret, JSON_PRETTY_PRINT);
}
?>