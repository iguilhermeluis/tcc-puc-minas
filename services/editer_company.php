<?php
    session_start();
    require_once "../connection/connection.php";
    require_once "../services/register_log.php";

    $username = $_POST['username'];
    $id = $_POST['id'];
    $isAdmin = $_SESSION['level_user'] > 1 ? true : false;
    $isMe =  $_SESSION['id'] == $id;
    $id_company = $_SESSION['id_company'];

    if($isAdmin || $isMe){
        $sql = "UPDATE company SET name = :username WHERE id = :id";
        $stmt = $pdo->prepare($sql);
        $stmt->bindparam(':username', $username, PDO::PARAM_STR); 
        $stmt->bindparam(':id', $id_company, PDO::PARAM_STR); 
 
        $stmt->execute();

        if ($stmt->rowCount()) { 
            if($_SESSION['id'] == $id){
                $_SESSION['username'] = $username;
                $_SESSION['level_user'] = $level_user;
            }
            create_log($pdo, "editou o nome da empresa para: $username");
          
            echo "<script> alert('Editado com sucesso.'); window.location.replace('../editar-empresa.php'); </script>";
        } else { 
            echo "<script>alert('Erro ao editar.'); window.location.replace('../editar-empresa.php'); </script>";
        }
      
    } else {
        echo "<script> window.location.replace('editar-empresa.php'); </script>";
    } 
  
?>  

