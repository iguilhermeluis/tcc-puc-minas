<?php
    session_start();
    require_once "../connection/connection.php";
    require_once "../services/register_log.php";

    $username = $_POST['username'];
    $id = $_POST['id'];
    $isAdmin = $_SESSION['level_user'] > 1 ? true : false;
 
    if($isAdmin){
        $sql = "UPDATE cost_center SET name = :name WHERE id = :id";
        $stmt = $pdo->prepare($sql);
        $stmt->bindparam(':name', $username, PDO::PARAM_STR); 
        $stmt->bindparam(':id', $id, PDO::PARAM_STR);
        $stmt->execute();

        if ($stmt->rowCount()) {
            create_log($pdo, "editou o centro de custo de id $id para $username");
            echo "<script> alert('Editado com sucesso.'); window.location.replace('../listar-cc.php'); </script>";
        } else { 
            echo "<script>alert('Erro ao editar.'); window.location.replace('../listar-cc.php'); </script>";
        }
      
    } else {
        echo "<script> window.location.replace('listar-cc.php'); </script>";
    } 
  
?>  

