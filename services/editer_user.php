<?php
    session_start();
    require_once "../connection/connection.php";
    require_once "../services/register_log.php";

    $username = $_POST['username'];
    $user_password = $_POST['password'];
    $level_user = $_POST['level_user'];
    $id = $_POST['id'];
    $isAdmin = $_SESSION['level_user'] > 1 ? true : false;
    $isMe =  $_SESSION['id'] == $id;

    if($isAdmin || $isMe){
        $sql = "UPDATE users SET username = :username, user_password = :user_password, level_user = :level_user WHERE id = :id";
        $stmt = $pdo->prepare($sql);
        $stmt->bindparam(':id', $id, PDO::PARAM_STR);
        $stmt->bindparam(':username', $username, PDO::PARAM_STR);
        $stmt->bindparam(':user_password', $user_password, PDO::PARAM_STR);
        $stmt->bindparam(':level_user', $level_user, PDO::PARAM_STR); 
        
        $stmt->execute();

        if ($stmt->rowCount()) { 
            if($_SESSION['id'] == $id){
                $_SESSION['username'] = $username;
                $_SESSION['level_user'] = $level_user;
            }
            create_log($pdo, "editou o tipo de fatura de id $id para $username");
            echo "<script> alert('Editado com sucesso.'); window.location.replace('../listar-usuarios.php'); </script>";
        } else { 
            echo "<script>alert('Erro ao editar.'); window.location.replace('../listar-usuarios.php'); </script>";
        }
      
    } else {
        echo "<script> window.location.replace('listar-usuarios.php'); </script>";
    } 
  
?>  

