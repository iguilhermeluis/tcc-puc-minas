<?php
    session_start();
    require_once "../connection/connection.php";
    require_once "../services/register_log.php";

    if(isset($_SESSION['authenticated']) && $_SESSION['level_user'] > 1){
        $username = $_POST['username']; 
        $level_user = $_POST['level_user'];
        $id_company = $_SESSION['id_company'];
    
        $sql = "INSERT INTO category (name, id_company) VALUES (:name, :id_company)";
        $stmt = $pdo->prepare($sql); 
        $stmt->bindparam(':name', $username, PDO::PARAM_STR); 
        $stmt->bindparam(':id_company', $id_company, PDO::PARAM_STR);
        $stmt->execute();
        if ($stmt->rowCount()) {
            create_log($pdo, "cadastrou a categoria $username"); 
            echo "<script> alert('Cadastrado com sucesso.'); window.location.replace('../listar-categorias.php'); </script>";
        } else { 
            echo "<script>alert('Erro ao cadastrar.'); window.location.replace('../listar-categorias.php'); </script>";
        }
      
    } else {
        echo "<script> window.location.replace('listar-categorias.php'); </script>";
    } 
  
?>  

