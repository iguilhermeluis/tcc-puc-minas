<?php
    session_start();
    require_once "../connection/connection.php";
    require_once "../services/register_log.php";

    if(isset($_SESSION['authenticated']) && $_SESSION['level_user'] > 1){
        $username = $_POST['username'];
        $user_password = $_POST['password'];
        $level_user = $_POST['level_user'];
        $id_company = $_SESSION['id_company'];
    
        $sql = "INSERT INTO users (username, user_password, level_user, id_company) VALUES (:username, :user_password, :level_user, :id_company)";
        $stmt = $pdo->prepare($sql); 
        $stmt->bindparam(':username', $username, PDO::PARAM_STR);
        $stmt->bindparam(':user_password', $user_password, PDO::PARAM_STR);
        $stmt->bindparam(':level_user', $level_user, PDO::PARAM_STR);
        $stmt->bindparam(':id_company', $id_company, PDO::PARAM_STR);
        $stmt->execute();
        if ($stmt->rowCount()) { 
            create_log($pdo, "cadastrou o usuário $username");
            echo "<script> alert('Cadastrado com sucesso.'); window.location.replace('../listar-usuarios.php'); </script>";
        } else { 
            echo "<script>alert('Erro ao cadastrar.'); window.location.replace('../listar-usuarios.php'); </script>";
        }
      
    } else {
        echo "<script> window.location.replace('listar-usuarios.php'); </script>";
    } 
  
?>  

