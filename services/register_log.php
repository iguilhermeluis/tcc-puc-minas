<?php  
    function create_log($pdo, $action){
        $id_company = $_SESSION['id_company'];
        $id_user = $_SESSION['id'];
        $sql = "INSERT INTO log (id_user, id_company, action, time_course) VALUES (:id_user, :id_company, :action, now())";
        $stmt = $pdo->prepare($sql); 
        $stmt->bindparam(':id_user', $id_user, PDO::PARAM_STR); 
        $stmt->bindparam(':id_company', $id_company, PDO::PARAM_STR);
        $stmt->bindparam(':action', $action, PDO::PARAM_STR);
        $stmt->execute();
    }
  
?>  

