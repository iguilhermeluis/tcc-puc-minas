<?php
    session_start();
    require_once "../connection/connection.php";
    $_POST = json_decode(file_get_contents("php://input"),true);
    
    $username = $_POST['email'];
    $user_password = $_POST['password'];
    $uuid = $_POST['uuid'];
    $name_company = "ARQUIVA AI - " . $username;
    
    $sql = "INSERT INTO company (name) VALUES (:name_company); INSERT INTO users (username, user_password, uuid, level_user, id_company) VALUES (:username, :user_password, :uuid, 2, LAST_INSERT_ID())";
    $stmt = $pdo->prepare($sql); 
    $stmt->bindparam(':name_company', $name_company, PDO::PARAM_STR);
    $stmt->bindparam(':username', $username, PDO::PARAM_STR);
    $stmt->bindparam(':user_password', $user_password, PDO::PARAM_STR);
    $stmt->bindparam(':uuid', $uuid, PDO::PARAM_STR);
    $stmt->execute();
    if ($stmt->rowCount()) { 
        http_response_code(200); 
    } else { 
        http_response_code(401); // Forbidden
    }
      
?>  

