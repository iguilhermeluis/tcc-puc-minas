<?php  
session_start();
require_once '../connection/connection.php';


$_POST = json_decode(file_get_contents("php://input"),true);

$password = strtoupper($_POST["password"]);
$email = strtoupper($_POST["email"]); 
$stmt = $pdo->prepare('SELECT u.id, u.username, u.level_user, u.uuid, u.id_company, c.name FROM users u, company c WHERE u.id_company = c.id AND UPPER(username) = :email AND UPPER(user_password) = :user_password AND u.active = 1 limit 1');
$stmt->bindparam(':email', $email, PDO::PARAM_STR);
$stmt->bindparam(':user_password', $password, PDO::PARAM_STR);
$stmt->execute(); 
$line = $stmt->fetchObject();

if ($stmt->rowCount() > 0) { 
    $_SESSION['id'] = $line->id; 
    $_SESSION['username'] = $line->username; 
    $_SESSION['level_user'] = $line->level_user;
    $_SESSION['uuid'] = $line->uuid;
    $_SESSION['id_company'] = $line->id_company;
    $_SESSION['name_company'] = $line->name;
    $_SESSION['authenticated'] = true;  
    http_response_code(200); 
} else {
    http_response_code(401); // Forbidden
}


?>