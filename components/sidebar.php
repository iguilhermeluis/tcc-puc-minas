 
<aside class="menu" id="menu" data-test="menu">
        <nav>  
            <?php
              if($_SESSION['level_user'] > 1){

                echo $page == "dashboard" ? '<li class="active" data-test="menu-dashboard">' : '<li data-test="menu-dashboard">';

                echo "<a href='dashboard.php' >
                  <span>
                    <img
                      src='assets/imgs/icon_dashboard.svg'
                      height='18'
                      width='18' /></span>Visão geral
                      </a>
                      </li>";
 

                echo $page == "documentos" ? '<li class="active" data-test="menu-documentos">' : '<li data-test="menu-documentos">';

                echo "<a href='listar-documentos.php' >
                  <span>
                    <img
                      src='assets/imgs/icon_doc.svg'
                      height='18'
                      width='18' /></span>Gerenciamento de documentos
                      </a>
                      </li>";


                echo $page == "faturas" ? '<li class="active" data-test="menu-nf">' : '<li data-test="menu-nf">';
                echo "<a href='listar-nf.php' >
                  <span>
                    <img
                      src='assets/imgs/barcode.svg'
                      height='18'
                      width='18' /></span>Gerenciamento de notas fiscais
                      </a>
                      </li>";

                echo $page == "categoria" ? '<li class="active" data-test="menu-categorias">' : '<li data-test="menu-categorias">';
                echo "<a href='listar-categorias.php' >
                  <span>
                    <img
                      src='assets/imgs/icon_category.svg'
                      height='18'
                      width='18' /></span>Categorias
                      </a>
                      </li>";

                echo $page == "centro de custo" ? '<li class="active" data-test="menu-cc">' : '<li data-test="menu-cc">';
                echo "<a href='listar-cc.php' >
                  <span>
                    <img
                      src='assets/imgs/icon_cc.svg'
                      height='18'
                      width='18' /></span>Centros de custos
                      </a>
                      </li>"; 

                echo $page == "tipo de fatura" ? '<li class="active" data-test="menu-tipo-fatura">' : '<li data-test="menu-tipo-fatura">';
                echo "<a href='listar-tipo-fatura.php' >
                  <span>
                    <img
                      src='assets/imgs/icon_cc.svg'
                      height='18'
                      width='18' /></span>Tipos faturas
                      </a>
                      </li>";

                echo $page == "tipo de documento" ? '<li class="active" data-test="menu-tipo-documento">' : '<li data-test="menu-tipo-documento">';
                echo "<a href='listar-tipo-documento.php' >
                    <span>
                    <img
                      src='assets/imgs/icon_doc.svg'
                      height='18'
                      width='18' /></span>Tipos documentos
                      </a>
                   </li>";


                echo $page == "departamento" ? '<li class="active" data-test="menu-departamento">' : '<li data-test="menu-departamento">';

                echo "<a href='listar-departamento.php' >
                    <span>
                      <img
                        src='assets/imgs/icon_dp.svg'
                        height='18'
                        width='18' /></span>Departamentos
                    </a>
                  </li>";
 
                echo $page == "usuarios" ? '<li class="active" data-test="menu-usuarios">' : '<li data-test="menu-usuarios">';

                echo"<a href='listar-usuarios.php' >
                  <span>
                    <img
                      src='assets/imgs/icon_usuarios.svg'
                      height='20'
                      width='20' /></span>
                     Usuários
                  </a>
                </li>";
              } 


              echo $page == 'perfil' ? '<li class="active" data-test="menu-perfil">' : '<li data-test="menu-perfil">';
 
              echo "<a href='editar-usuario.php?id=". $_SESSION['id'] . "&profile=true' >
                <span
                  ><img
                    src='assets/imgs/icon_edit_profile.svg'
                    height='18'
                    width='18'/></span>Editar perfil
                </a>
              </li>";

              echo $page == 'editar empresa' ? '<li class="active"  data-test="menu-editar-empresa">' : '<li data-test="menu-editar-empresa">';
              echo "<a href='editar-empresa.php' data-test='menu-editar-empresa'>
              <span
                ><img
                  src='assets/imgs/icon_company.svg'
                  height='18'
                  width='18'/></span>Editar empresa
              </a>
            </li>";

            echo $page == 'logs' ? '<li class="active">' : '<li>';
            echo "<a href='logs.php' data-test='menu-logs'>
            <span
              ><img
                src='assets/imgs/icon_company.svg'
                height='18'
                width='18'/></span>Ver logs
            </a>
          </li>"; 
            ?> 
         
            <li data-test='menu-logout'>
              <a href="logout.php"  >
              <span
                ><img
                  src="assets/imgs/icon_logout.svg"
                  height="18"
                  width="18" /></span
              >Sair
              </a>
            </li>
          </ul>
        </nav>
      </aside>