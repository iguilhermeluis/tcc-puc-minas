# Arquiva-ai - TCC PUC-Minas

O Arquiva-ai é um projeto de Trabalho de Conclusão de Curso da PUC-Minas que tem como objetivo aprimorar o acesso aos documentos para empresas que desejam digitalizar seus arquivos, permitindo que seus colaboradores possam visualizá-los de forma instantânea e simplificada em qualquer dispositivo. Além dos benefícios para as empresas e seus colaboradores, a digitalização de documentos também pode trazer benefícios ambientais, reduzindo a necessidade de impressão em papel e contribuindo para a preservação do meio ambiente.


## Tecnologias Utilizadas

Linguagem de programação: PHP & Javascript <br>
Banco de dados: MySQL<br>
Biblioteca para testes: Cypress<br>
Biblioteca para autenticação: Firebase Auth<br>

## Instruções de Instalação

* Crie um banco de dados usando o script database.sql
* Abra o arquivo application/config/database.php e configure com os acessos do seu banco de dados
* Acesse o site

[Protótipo Figma](https://www.figma.com/file/VzoKEaM7BABWDui7AvJ5U3/arquiva-ai---tcc-puc-minas?node-id=0%3A1&t=NDIzmoZMfi48Qynv-1)
