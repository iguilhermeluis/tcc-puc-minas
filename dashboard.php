<?php
  session_start();
  $page = "dashboard";
  require_once "connection/connection.php";

  if(!isset($_SESSION['authenticated'])){
    echo "<script> window.location.replace('index.php'); </script>";
    session_destroy(); 
  } 
   
?>
<!DOCTYPE html>
<html lang="pt-BR">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Arquiva Ai - Dashboard</title>
        <?php include 'components/favicon.php'; ?><link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link
      href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;700&display=swap"
      rel="stylesheet"
    />
    <link
      rel="stylesheet"
      href="https://use.fontawesome.com/releases/v5.7.0/css/all.css"
      integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ"
      crossorigin="anonymous"
    />

    <link rel="stylesheet" href="bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.2/css/jquery.dataTables.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />

    
    <link rel="stylesheet" href="css/estilo.css" />

    <style>
        table, th, td {
        border: 1px solid var(--border-color);
        border-collapse: collapse;
        padding: 10px;
        }
        table a {
            margin-right: 10px;
        }
        </style>
  </head>
  <body>
    <?php require_once "components/header.php"; ?>

    <main class="container-main">
    <?php require_once "components/sidebar.php"; ?>
    <div class="box-content">
      <div class="container-list spacing-header"> 
        <div class="panel">
          <h2>Visão Geral</h2>
          <br/>
          <div class="loader-box">
            <div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
          </div>

          <div class="content box-chart">
            <div id="canvas-holder" style="width:40%">
              <canvas id="chart-area" />  
            </div>   
            <div id="canvas-holder" style="width:40%">
              <canvas id="chart-bar" /> 
            </div>    
          </div>
        </div>
      </div>
    </div>
    </main>
    <div id="snackbar"></div>
    <script src="js/script.js"></script>
    <script src="js/chart.js"></script>
    <script src="js/utils.js"></script>
 
    
  <script>
    
    <?php 
        $id_company = $_SESSION['id_company'];
        $sql = "SELECT COUNT(id) AS qt FROM document WHERE id_company = $id_company";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
 
        $documents = $result['qt'];
        $sql = "SELECT COUNT(id) AS qt FROM invoices WHERE id_company = $id_company";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        $invoices = $result['qt'];
    ?>

    var config = {
        type: 'pie',
        data: {
            datasets: [{
                data: [
                    <?php echo $documents ?>,
                    <?php echo $invoices ?>,
                ],
                backgroundColor: [
                    window.chartColors.blue,
                    window.chartColors.red,
                ],
                label: 'Dataset 1'
            }],
            labels: [
                "Documentos",
                "Faturas",
            ]
        },
        options: {
            responsive: true
        }
    };

    var config2 = {
        type: 'bar',
        labels: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio'],
        data: {
            datasets: [{
                data: [
                    <?php echo $documents ?>,
                    <?php echo $invoices ?>,
                ],
                backgroundColor: [
                  window.chartColors.blue,
                  window.chartColors.red,
                ],
                label: 'Documentos'
            }],
            labels: [
                "Documentos",
                "Faturas",
            ]
        },
        options: {
            responsive: true
        }
    };


    window.onload = function() {
        var ctx = document.getElementById("chart-area").getContext("2d");
        var ctxBar = document.getElementById("chart-bar").getContext("2d");
        window.myPie = new Chart(ctx, config);
        window.myBar = new Chart(ctxBar, config2);
    };
 

    var colorNames = Object.keys(window.chartColors);

  </script>
 
</body>
</html>
