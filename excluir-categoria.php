<?php
    session_start();

    require_once "connection/connection.php";
    if($_SESSION['level_user'] > 1){
        $id = $_GET['id'];
        $sql = "UPDATE category SET active = 0 WHERE id = :id";
    
        $stmt = $pdo->prepare($sql);
        $stmt->bindparam(':id', $id, PDO::PARAM_STR); 
        $stmt->execute();

        if ($stmt->rowCount()) { 
            echo "<script> window.location.replace('listar-categorias.php'); </script>";
        } else { 
            echo "<script>alert('Erro ao excluir'); window.location.replace('listar-categorias.php'); </script>";
        }
      
    } else {
        echo "<script> window.location.replace('listar-categorias.php'); </script>";
    }

  
?>  